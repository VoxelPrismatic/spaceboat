const Discord = require('discord.js');
const utils = require('../utils');

module.exports.run = async (bot, message, args) => {
    let target = message.mentions.members.first() || message.guild.members.cache.get(args[0]);
    const sendAvatar = (person) => {
        message.channel.send(new Discord.MessageEmbed().setImage(person.displayAvatarURL()).setColor(utils.botColour).setDescription(`Here's <@${person.id}>'s avatar!`));
    };
    sendAvatar(target ? target.user : message.author);
};

module.exports.help = {
    name: "avatar",
    usage: 'avatar [mention/user id]',
    type: 'Fun',
    description: 'Returns your or another person\'s avatar!',
};

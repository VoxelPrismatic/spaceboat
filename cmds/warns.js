const utils = require('../utils');
const guilds = require('../data/guilds.json');

module.exports.run = async (bot, message, args) => {
    const { guild } = message;
    const guildID = guild.id;
    //import logChannel.
    const logChannel = guild.channels.cache.get(guilds[guildID].logChannelID);
    let sql = `SELECT warn_id,
                      guild_id, 
                      user_id, 
                      moderator_id, 
                      warn_str, 
                      severity, 
                      date 
               FROM warnings
               ORDER BY warn_id`;
    let sql_all = `SELECT warn_id,
                          guild_id, 
                          user_id, 
                          moderator_id, 
                          warn_str, 
                          severity, 
                          date 
                   FROM warnings
                   ORDER BY user_id, warn_id`;

    let sql_users = `SELECT DISTINCT user_id
                     FROM warnings
                     WHERE guild_id = ?
                     ORDER BY user_id`;

    let sql_warnid = `SELECT warn_id
                      FROM warnings
                      WHERE warn_id = ?`;

    function getusers() {
        return new Promise((resolve, reject) => {
            utils.db.all(sql_users, [guildID], (err, rows) => {
                if (err) return reject(err);
                else {
                    resolve(rows);
                }
            });
        });
    }

    let users = await getusers().catch(err => console.log(err));

    function checkwarnid(warnid) {
        return new Promise((resolve, reject) => {
            utils.db.all(sql_warnid, [warnid], (err, rows) => {
                if (err) reject(err);
                else {
                    resolve(rows);
                }
            });
        });
    }

    if (message.channel.id === guilds[guildID].adminbotChannelID) {
        if (!args[0]) { // Sends an embed with a summarized report for all warned users in a guild.
            const usersAndWarns = await Promise.all(
                users.map(async user =>
                    [user, await utils.getWarns(guildID, user.user_id).catch(err => console.log(err))])).catch(err => console.log(err)) || [];
            const embedFields = usersAndWarns.map(([user, warns]) => {
                const latestWarn = warns[warns.length - 1];
                const date = (new Date(latestWarn.date)).toDateString();
                let target;
                try {
                    target = guild.members.cache.get(user.user_id).user.tag;
                } catch (error) {
                    console.log(error.message);
                } finally {
                    target = target || user.user_id;
                }
                return {
                    name: target,
                    value: `**Warn Severity Tally:** ${utils.getWarnsTally(warns)}\n**Most Recent Warning:** (id: ${latestWarn.warn_id})\n**Severity:** ${latestWarn.severity}\n**Warning:** "${latestWarn.warn_str}"\n**Mod:** ${message.guild.members.cache.get(latestWarn.moderator_id)}\n**Date:** ${date}\n_ _`,
                    inline: true,
                };
            });
            await utils.sendPagedEmbeds(message.channel, 'List of warned users', embed => embed.setDescription('_ _'), embedFields);
        } else if (args[0] == 'full') { // Sends an embed with all warnings for each warned user in the guild.
            for (let user of users) {
                let target, embedAuthorName, embedAuthorIconURL;
                try { target = guild.members.cache.get(user.user_id); }
                catch (error) { console.log(error.message); }
                finally {
                    if (target) { embedAuthorName = target.user.username; embedAuthorIconURL = target.user.displayAvatarURL(); }
                    else { embedAuthorName = user.user_id; }
                }
                const warns = await utils.getWarns(guildID, user.user_id).catch(err => console.log(err));
                const embedDesc = `Warning Severity Tally: ${utils.getWarnsTally(warns)}`;
                const embedFields = warns.map(warn => ({
                    name: `**Warn ID:** ${warn.warn_id}`,
                    value: `\n**Severity:** ${warn.severity}\n**Mod:** ${guild.members.cache.get(warn.moderator_id)}\n**Warning:** "${warn.warn_str}"\n**Date:** ${(new Date(warn.date)).toDateString()}\n`,
                    inline: true,
                }));
                await utils.sendPagedEmbeds(message.channel, '', embed => embed.setAuthor(embedAuthorName, embedAuthorIconURL).setDescription(embedDesc), embedFields);
            }
        } else if (args[0] == 'config') {
            if (args[1] == 'severity') {
                const warns = await checkwarnid(args[2]);
                if (!warns || warns.length === 0) {
                    const response = utils.randomSelection([
                        `Which warn id do you want me to configure?`,
                        `That's not a valid warn id...`,
                        `Please can you state which warn id you want configured?`,
                    ]);
                    return message.channel.send(response);
                } else {
                    const severity = parseInt(args[3], 10);
                    if (args[3] && !isNaN(severity) && severity > 0 && severity < 11) {
                        utils.db.run(`UPDATE warnings
                                SET severity = ?
                                WHERE warn_id = ?`,
                            [args[3], args[2]], err => { if (err) console.log(err) });
                        return message.channel.send(`Severity updated!`);
                    } else {
                        const response = utils.randomSelection([
                            `That isn't a valid severity value.`,
                            `Can you make sure that severity value is a number between 1 and 10 please?`,
                        ]);
                        return message.channel.send(response);
                    }
                }
            } else if (args[1] == 'warning') {
                const warns = await checkwarnid(args[2]);
                if (!warns || warns.length === 0) {
                    const response = utils.randomSelection([
                        `Which warn id do you want me to configure?`,
                        `That's not a valid warn id...`,
                        `Please can you state which warn id you want configured?`,
                    ]);
                    return message.channel.send(response);
                } else {
                    if (args[2] == `warning`) {
                        if (args[3]) {
                            utils.db.run(`UPDATE warnings
                                    SET warn_str = ?
                                    WHERE warn_id = ?`,
                                [args.slice(3).join(' '), `${args[1]}`], err => { if (err) console.log(err); })
                            return message.channel.send(`Warning text updated!`);
                        } else {
                            const response = utils.randomSelection([
                                `Please can you supply a warning string?`,
                                `Please input what you want to change the warning to.`,
                            ]);
                            return message.channel.send(response);
                        }
                    }
                }
            }
            const warns = await checkwarnid(args[1]);
            if (warns && warns.length > 0) {
                if (args[2] == `severity`) {
                    const severity = parseInt(args[3], 10);
                    if (args[3] && !isNaN(severity) && severity > 0 && severity < 11) {
                        utils.db.run(`UPDATE warnings
                                SET severity = ?
                                WHERE warn_id = ?`,
                            [args[3], args[1]], err => { if (err) console.log(err); })
                        return message.channel.send(`Severity updated!`);
                    } else {
                        const response = utils.randomSelection([
                            `That isn't a valid severity value.`,
                            `Can you make sure that severity value is a number between 1 and 10 please?`,
                        ]);
                        return message.channel.send(response);
                    }
                } else if (args[2] == `warning`) {
                    if (args[3]) {
                        utils.db.run(`UPDATE warnings
                                SET warn_str = ?
                                WHERE warn_id = ?`,
                            [args.slice(3).join(' '), `${args[1]}`], err => { if (err) console.log(err) });
                        return message.channel.send(`Warning text updated!`);
                    } else {
                        const response = utils.randomSelection([
                            `Please can you supply a warning string?`,
                            `Please input what you want to change the warning to.`,
                        ]);
                        return message.channel.send(response);
                    }
                }
                else {
                    const response = utils.randomSelection([
                        `That's not a valid configuration parameter`,
                        'Please can you state whether you want to configure the `severity` or the `warning` text of the warning?',
                    ]);
                    return message.channel.send(response);
                }
            } else {
                const response = utils.randomSelection([
                    `Errrr I don't know what to do with this...`,
                    `Erm... Not sure how to handle this...`,
                    `That's not really how this command is meant to be used...`,
                    `Please can you include the warning id what you want to change and what parameter (severity or warning text) you want to change.`,
                ]);
                return message.channel.send(response);
            }
        } else if (args[0] == `remove` || args[0] == `delete` || args[0] == `del` || args[0] == `rm`) {
            const warns = await checkwarnid(args[1]);
            if (!warns || warns.length === 0) {
                const response = utils.randomSelection([
                    `Which warn id do you want me to remove?`,
                    `That's not a valid warn id...`,
                    `Please can you state which warn id you want removed?`,
                ]);
                return message.channel.send(response);
            } else {
                utils.db.run(`DELETE FROM warnings WHERE warn_id = ?`, args[1], err => { if (err) console.log(err) });
                const response = utils.randomSelection([
                    `Done and dusted! :D`,
                    `Warning removed.`,
                    `Done. :)`,
                ]);
                return message.channel.send(response);
            }
        } else {
            const target = message.mentions.members.first() || guild.members.cache.get(args[0]); // get mentioned member
            let targetUserID, embedAuthorName, embedAuthorIconURL;
            if (target) {
                targetUserID = target.id;
                embedAuthorName = target.user.username;
                embedAuthorIconURL = target.user.displayAvatarURL();
            } else {
                targetUserID = args[0];
                embedAuthorName = args[0];
            }
            const warns = await utils.getWarns(guildID, targetUserID).catch(err => console.log(err));
            const embedDesc = `Warning Severity Tally: ${utils.getWarnsTally(warns)}`;
            const embedFields = warns.map(warn => ({
                name: `**Warn ID:** ${warn.warn_id}`,
                value: `\n**Severity:** ${warn.severity}\n**Mod:** ${guild.members.cache.get(warn.moderator_id)}\n**Warning:** "${warn.warn_str}"\n**Date:** ${(new Date(warn.date)).toDateString()}\n`,
                inline: true,
            }));
            await utils.sendPagedEmbeds(message.channel, '', embed => embed.setAuthor(embedAuthorName, embedAuthorIconURL).setDescription(embedDesc), embedFields);
        }
    }
};

module.exports.help = {
    name: 'warns',
    usage: 'warn [<config>] [<user>]',
    type: 'Moderation',
    description: 'Allows the viewing',
};

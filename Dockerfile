FROM node:12.18.2-alpine3.11
WORKDIR /app
RUN apk add --update --no-cache build-base python
COPY package.json yarn.lock ./
RUN yarn install
COPY . .

# note: -S means system group/user
RUN addgroup -S spaceboatg && adduser -S -G spaceboatg spaceboat
RUN chown spaceboat:spaceboatg /app
USER spaceboat

CMD ["yarn", "start-direct"]

const Discord = require('discord.js');
const guilds = require('./data/guilds.json');
const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./data/data.db', sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE, (err) => {
    if (err) return console.error(err.message);
    else console.log(`Connected to Database.`);
});

const sql_warn = `SELECT warn_id,
    moderator_id,
    warn_str,
    severity,
    date
    FROM warnings
    WHERE user_id = ?
    AND guild_id = ?
    ORDER BY warn_id`;

const timeUnitsByAlias = (() => {
    const units = {};
    for (const [aliases, unit] of [
        [['seconds', 'second', 'secs', 'sec', 's'], ['second',                      1 * 1e3]],
        [['minutes', 'minute', 'mins', 'min', 'm'], ['minute',                 60 * 1 * 1e3]],
        [['hours', 'hour', 'h'],                    ['hour',              60 * 60 * 1 * 1e3]],
        [['days', 'day', 'd'],                      ['day',          24 * 60 * 60 * 1 * 1e3]],
        [['weeks', 'week', 'w'],                    ['week',     7 * 24 * 60 * 60 * 1 * 1e3]],
        [['months', 'month', 'mon'],                ['month',   30 * 24 * 60 * 60 * 1 * 1e3]],
        [['years', 'year', 'y'],                    ['year',   365 * 24 * 60 * 60 * 1 * 1e3]],
    ]) {
        for (const alias of aliases) {
            units[alias] = unit;
        }
    }
    return units;
})();

const botColour = '678ade';

exports.logChannel = (bot, guildID, colour, event, user, moderator, reason, timeString, other, thumbnail) => {
    if (!guildID) return console.log(`guildID is not defined!`);
    // identify the logger channel
    let logChannel = (bot.guilds.cache.get(guildID)).channels.cache.get(guilds[guildID].logChannelID);
    // check if other supplied
    other = other || '';    // check for a user parameter
    let icon, userString, user_id;
    if (user) {
        userString = `**Target:** ${user} (${user.tag})`;
        user_id = `ID: ${user.id}`;
        icon = user.displayAvatarURL();
    }
    if (!user || user == '') {
        userString = '';
        user_id = '';
    }
    // check if there is a moderator parameter
    if (moderator) {
        var moderatorString = `\n**Moderator:** ${moderator} (${moderator.tag})`;
        if (!user) { icon = moderator.displayAvatarURL(); }
    }
    if (!moderator || moderator == '') {
        var moderatorString = '';
        if (!user) { icon = bot.user.displayAvatarURL(); }
    }
    // check for reason
    if (reason) { var reasonString = `\n**Reason:** ${reason}` }
    if (!reason || reason == '') { var reasonString = '' }
    // check for timeString
    timeString = timeString || '';
    // do the actual embed
    try {
        logChannel.send({
            embed: new Discord.MessageEmbed()
                .setDescription(`**Target:** ${user} (${user.tag})${moderatorString}${reasonString}${timeString}${other}`)
                .setFooter(`ID: ${user.id}`)
                .setAuthor(event, user.displayAvatarURL())
                .setTimestamp()
                .setThumbnail(thumbnail)
                .setColor(colour)
        });
    }
    catch (error) {
        if (!guildID) return console.log(`LoggerERR: No guildID given!`);
        if (!logChannel) return console.log(`LoggerERR: No logchannel defined for this guild!`);
        console.log(error);
    }
};
exports.randomSelection = (choices) => {
    return choices[Math.floor(Math.random() * choices.length)];
};
exports.warning = async (bot, guildID, userID, moderatorID, string, severity, callback) => {
    // create warnings table if it doesn't exist
    function createtable(cb) {
        db.run(`CREATE TABLE IF NOT EXISTS warnings 
        (warn_id integer PRIMARY KEY, 
            guild_id text, 
            user_id text, 
            moderator_id text, 
            warn_str text, 
            severity integer, 
            date integer)`,
        (err) => {
            if (err) {
                cb(err.message);
            } else {
                console.log(`Warnings table opened/created successfully.`);
                cb(null);
            }
        })
    }
    // perform the insertion
    function insertwarning(guildID, userID, moderatorID, string, severity, cb) {
        db.run(
            "INSERT INTO warnings (guild_id, user_id, moderator_id, warn_str, severity, date) VALUES(?,?,?,?,?,?)", 
            [guildID, userID, moderatorID, string, severity, Date.now()], 
            function (err) {
            if (err) {
                cb(err.message, null)
            } else {
                console.log(`Warning insertion successful. Warning ID: ${this.lastID}`);
                cb(null, this.lastID)
            }
        })
    }
    createtable((err) => {
        if (err) return console.log(err);
        insertwarning(guildID, userID, moderatorID, string, severity, (err, result) => {
            if (err) {
                console.log(err);
                callback(err, null);
                return;
            } else callback(err, result);
        })
    });
};
exports.getWarns = (guildID, userID) => {
    return new Promise((resolve, reject) => {
        db.all(sql_warn, [userID, guildID], (err, rows) => {
            if (err) reject(err);
            else {
                resolve(rows);
            }
        });
    })
};
exports.sendPagedEmbeds = async (textBasedChan, embedTitlePrefix, embedConfigurer, fields) => {
    const pageSize = 25;
    const numPages = Math.max(Math.ceil(fields.length / pageSize), 1);
    const messagesToSend = [];
    for (let i = 0; i < numPages; i++) {
        const embed = new Discord.MessageEmbed({
            title: `${embedTitlePrefix}${numPages > 1 ? `${embedTitlePrefix ? ' ' : ''}(page ${i + 1} of ${numPages})` : ''}`,
        }).setColor(botColour);
        if (embedConfigurer) {
            embedConfigurer(embed);
        }
        embed.addFields(fields.slice(i * pageSize, (i + 1) * pageSize));
        messagesToSend.push({ embed });
    }
    for (const m of messagesToSend) {
        // yes, await in a loop (send pages 1 at a time)
        await textBasedChan.send(m);
    }
};
// returns [name, numMillis] of the parsed time unit, or null if unrecognized
exports.parseTimeUnit = (str) => timeUnitsByAlias[str.toLowerCase()] || null;
exports.getWarnsTally = (warns) => warns.reduce((acc, cur) => acc + cur.severity, 0);
exports.thanos = ['144670992768172032', '166125035092836352'];
exports.botColour = botColour;
exports.colours = {
    'green': '43b581',
    'red': 'f04947',
    'orange': 'ffac33',
    'purple': 'aa8ed6',
    'blue': '3b88c3',
    'darkblue': '226699',
    'black': '292f33',
    'white': 'ffffff'
};
exports.db = db;
